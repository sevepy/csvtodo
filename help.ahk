﻿; Generated by AutoGUI 2.6.2
#SingleInstance Force
#NoEnv
SetWorkingDir %A_ScriptDir%
SetBatchLines -1

#Include %A_ScriptDir%\ControlColor.ahk
;MsgBox, % "AutoHotkey version: " A_AhkVersion
Gui +hWndhMainWnd
Gui Color, 0x008000
Gui Add, Picture, x39 y30 w188 h142, %A_ScriptDir%\csvtodo.png
Gui Font, s14 cWhite, Microsoft Sans Serif
Gui Add, Text, hWndhTxtCsvtodo x287 y22 w268 h40 +0x200, CSVTodo v. 24.01
Gui Font, s10, Microsoft Sans Serif
Gui Add, Button, gPlanner x287 y65 w225 h90, Integrated your hybrid planning and notetaking with ISO8601 WEEKLY PLANNER NOTEBOOK
Gui Font
ControlColor(hTxtCsvtodo, hMainWnd, 0x008000, 0xFFFFFF)
Gui Font, s15, Microsoft Sans Serif
Gui Add,  Button, gClose x287 y255 w225 h38, Close
Gui Font
Gui Font, s13 cWhite, Microsoft Sans Serif
Gui Add, Text, x287 y155 w267 h40 +0x200, Todo App. 
Gui Font
Gui Font, s13 cWhite, Microsoft Sans Serif
Gui Add, Text, x287 y195 w264 h40 +0x200, 2023 S. Tessarin (c)
Gui Font
Gui Add, Link, x147 y257 w52 h0, <a href="https://autohotkey.com">autohotkey.com</a>
Gui Font, s16 cBlack, Microsoft Sans Serif

Gui Font
Gui Font, s15, Microsoft Sans Serif
Gui Add, Button, gSupport x29 y257 w225 h38, Support this project.
Gui Font

Gui Show, w593 h350, Help
Return
Support:
Run, https://www.buymeacoffee.com/seve
Return
Planner:
Run, https://a.co/d/i5RRXuU
Return
Close:
    ExitApp
GuiEscape:
GuiClose:
    ExitApp

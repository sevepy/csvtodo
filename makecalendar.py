## Copyright © 2023 Seve Tessarin. All rights reserved.
import sqlite3
import calendar
import holidays
import locale
import pdb
from datetime import datetime, timedelta

locale.setlocale(locale.LC_ALL, 'en_EN')
# Connect to the database
conn = sqlite3.connect('calendarUS.db')

# Create a table with four columns
conn.execute('''CREATE TABLE calendar
             (
             ID INTEGER PRIMARY KEY,
             D DATE NOT NULL UNIQUE,
             YEAR INT NOT NULL,
             MONTH INT NOT NULL,
             MONTHNAME TEXT NOT NULL,
             MONTHABBR TEXT NOT NULL,
             DAY INT NOT NULL,
             DAYNAME TEXT NOT NULL,
             DAYABBR TEXT NOT NULL,
             QUARTER INT NOT NULL,
             ISOYEAR INT NOT NULL,
             ISOWEEK INT NOT NULL,
             ISODAY INT NOT NULL,
             DAYOFTHEWEEK INT NOT NULL,
             ISLEAP INT NOT NULL,
             ISHOLIDAY INT NOT NULL,
             HOLIDAYDESC TEXT
             );''')

# Fill the table with dates starting from January 1st, 1970 until December 31st, 2040
start_date = datetime(1970, 1, 1)
end_date = datetime(2040, 12, 31)
delta = timedelta(days=1)
holidays = holidays.country_holidays('US')

while start_date <= end_date:
    day_of_week = calendar.weekday(start_date.year, start_date.month, start_date.day)
    year = start_date.year
    month = start_date.month # 1-12
    monthname = calendar.month_name[month] # months of the year in the current locale
    monthabbr = calendar.month_abbr[month]
    day = start_date.day     # 1-31
    dayname = calendar.day_name[day_of_week]
    dayabbr = calendar.day_abbr[day_of_week]
    # pdb.set_trace()
    # Determine which quarter of the year belongs to
    quarter = (month - 1) // 3 + 1
    isocal = start_date.isocalendar()
    isoyear = isocal.year
    isoweek = isocal.week
    isoday = day_of_week + 1 # 1 = Mon ... 7 = Sunday
    isleap = int(calendar.isleap(year))
    id = year * 10000 + month * 100 + day
    # Determine if  a holiday
    if start_date in holidays:
        isholiday = 1
        holidaydesc = holidays.get(start_date)
        
    else:
        isholiday = 0
        holidaydesc = ""
    
    # pdb.set_trace()
    sql_query = f"INSERT INTO calendar (ID, D, YEAR, MONTH, MONTHNAME,  MONTHABBR, DAY, DAYNAME, DAYABBR, QUARTER, ISOYEAR, ISOWEEK,  ISODAY, DAYOFTHEWEEK, ISLEAP, ISHOLIDAY, HOLIDAYDESC) VALUES ({id}, '{start_date.date()}', {year}, {month}, '{monthname}', '{monthabbr}', {day}, '{dayname}', '{dayabbr}', {quarter}, {isoyear}, {isoweek}, {isoday}, {day_of_week}, {isleap}, {isholiday}, \"{holidaydesc}\")"
    conn.execute(sql_query)
    start_date += delta

# Commit the transaction
conn.commit()

# Close the connection
conn.close()




; todo.ahk: An AutoHotKey GUI for working with todo.csv files.

; Copyright 2023 S. Tessarin

; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases
#SingleInstance Force 
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability
#Include Anchor.ahk
#Include CSV.ahk
#Include textrender.ahk
#Include Time.ahk

RegRead, locale, HKEY_CURRENT_USER, Control Panel\International, locale
; Setting data time regional formatting en-IE
RegRead, localename, HKEY_CURRENT_USER, Control Panel\International, localename


INI_FILE_NAME := "csvtodo.ini"
TODO_FILE_NAME := "todo.csv"
DONE_FILE_NAME := "done.csv"
ICON_FILE_NAME := "csvtodo.ico"

SCRIPT_HOTKEY := GetConfig("UI","ScriptHotkey","#t")

ICON_PATH := A_ScriptDir . "\\" . ICON_FILE_NAME
INI_PATH := A_ScriptDir . "\\" . INI_FILE_NAME
EnvGet, vHomeDrive, HOMEDRIVE
EnvGet, vHomePath, HOMEPATH
HOMEPATH := vHomeDrive . vHOMEPATH 
TEXT_EDITOR := GetConfig("Files","Editor","notepad.exe")
ANNOTATION_PATH := GetConfig("Files","AnnotationDir", "\Documents" )
ANNOTATION_PATH := HOMEPATH . ANNOTATION_PATH
WINDOW_TITLE := "CSVTodo"

DIARY_PATH := GetConfig("Diary","DiaryDir", "\diary" )
DIARY_PATH := HOMEPATH . DIARY_PATH

Header:="id,status,priority,project,description,context,start date,due date,annotation"

TODO_PATH := GetPath("todo", TODO_FILE_NAME)
DONE_PATH := GetPath("done", DONE_FILE_NAME)


; '&' underlines the next letter, enabling the user to press ALT+N to focus on the following text field.
ADD_LABEL := "&New:"
PROJECT_LABEL := "&Project:"
CONTEXT_LABEL := "&Context/Tag:"
FILTER_LABEL:= "&Filter:"
DUE_DATE_LABEL := "&Due date:"
START_DATE_LABEL := "&Start date:"
ITEMS_LABEL := "&Items:"
FILE_LABEL := "&Task File:"
LINE_NUM_LABEL := "&Line #:"


FILE_LIST := GetConfig("Files","FileList","todo")



; columns position in the LV
SUBTASK_COLUMN := 3
TEXT_COLUMN := 4
LINE_COLUMN := 5
DUE_DATE_COLUMN := 7
START_DATE_COLUMN := 6
ANNOTATION_COLUMN := 8
PRIORITY_CALC_HEADER := 9 
; columns position in the CSV file
CSV_POS_ID := 1 
CSV_POS_STATUS := 2
CSV_POS_PRIORITY := 3
CSV_POS_PROJECT := 4
CSV_POS_DESCRIPTION := 5
CSV_POS_CONTEXT := 6
CSV_POS_STARTDATE := 7
CSV_POS_DUEDATE := 8
CSV_POS_ANNOTATION := 9



STATUS_HEADER := "Status"
TEXT_HEADER := "Description"
SUBTASK_HEADER := "Subtask"
PRIORITY_HEADER := "Priority"
ANNOTATION_HEADER := "Notes"
LINE_NUMBER_HEADER := "Line #"
DUE_DATE_HEADER := "Due Date"
START_DATE_HEADER := "Start Date"
PRIORITY_CALC_HEADER := "Priority Hidden" ; hidden column

ADD_BUTTON_TEXT := "Add"
TODAY_BUTTON_TEXT := "Today"
SHOWTODO_BUTTON_TEXT := "Display"
CLARFILTER_BUTTON_TEXT := "Clear"
ISO8601_BUTTON_TEXT := "ISO"
LONG_CONTROL_WIDTH := GetConfig("UI","ListWidth",600)

SHORT_CONTROL_WIDTH := LONG_CONTROL_WIDTH/5
TEXT_WIDTH := 50
LIST_WIDTH := LONG_CONTROL_WIDTH + TEXT_WIDTH
LIST_HEIGHT := GetConfig("UI","ListHeight",300)

LINE_NUM_WIDTH := 50

UPDATE_PROMPT := "What do you want to change ""`%text`%"" to?"
DELETE_PROMPT := "Are you sure you want to delete ""`%text`%""?"

CONTROL_FONT := GetConfig("UI","GuiFont","San Serif")
FONT_SIZE := GetConfig("UI","FontSize","9")


; Set icon.
Menu TRAY, Icon, %ICON_PATH%


; Define the GUI.
Gui +Resize +MinSize -MaximizeBox  
Gui, Font, s8, Tahoma
Gui, Margin , 3, 3
; Line 1 in GUI
Gui Add, Text, w%TEXT_WIDTH% x10 Section, %ADD_LABEL%
Gui Add, Edit, ys vNewItem W%LONG_CONTROL_WIDTH%
; Line 2 in GUI
Gui Add, Text, w%TEXT_WIDTH% x10 Section, %PROJECT_LABEL%
Gui Add, ComboBox, ys vProject W%SHORT_CONTROL_WIDTH%
Gui Add, Text, ys w%TEXT_WIDTH%, %CONTEXT_LABEL%
Gui Add, ComboBox, ys vContext W%SHORT_CONTROL_WIDTH%
; Line 3 in GUI
Gui Add, Text, w%TEXT_WIDTH% x10 Section, %START_DATE_LABEL%
Gui Add, DateTime, ys gSelectDateTime vStartDate ChooseNone W%SHORT_CONTROL_WIDTH%, yyyy-MM-dd

Gui Add, Text, w%TEXT_WIDTH% x10 Section, WDays:
Gui Add, Edit, ys W%SHORT_CONTROL_WIDTH% gEventHandler vMyEditControl
Gui Add, UpDown, ys vWDayUpDown W%SHORT_CONTROL_WIDTH% , 0
Gui Add, Text, w%TEXT_WIDTH% x10 Section, %DUE_DATE_LABEL%
Gui Add, DateTime, ys gSelectDateTime vDueDate ChooseNone W%SHORT_CONTROL_WIDTH%, yyyy-MM-dd
Gui Add, Button, ys Section, %TODAY_BUTTON_TEXT%
Gui Add, Button, ys Section, %SHOWTODO_BUTTON_TEXT%
Gui Add, Button, ys Section, %ISO8601_BUTTON_TEXT%
Gui Add, Button, ys Section, %CLARFILTER_BUTTON_TEXT%



; Line 4 in GUI
Gui Add, Text, w%TEXT_WIDTH% x10 Section, %FILTER_LABEL%
Gui Add, Edit, vFilter gFilter ys W%LONG_CONTROL_WIDTH%

Gui Add, Text, w1 x1 Section,
; Set font.
Gui, font, s%FONT_SIZE%, %CONTROL_FONT%
Gui, Margin,15,15,15,20 
Gui Add, ListView, vItems gItems AltSubmit -Multi ys H%LIST_HEIGHT% W%LIST_WIDTH%, %STATUS_HEADER%|%PRIORITY_HEADER%|%SUBTASK_HEADER%|%TEXT_HEADER%|%LINE_NUMBER_HEADER%|%START_DATE_HEADER%|%DUE_DATE_HEADER%|%ANNOTATION_HEADER%|%PRIORITY_CALC_HEADER%
Gui, font, s8, Tahoma

Gui Add, Button, vAdd gAdd Default Section, %ADD_BUTTON_TEXT%



; Fix sorting in columns with numbers.
LV_ModifyCol(LINE_COLUMN , "Logical")
LV_ModifyCol(DUE_DATE_COLUMN , "Logical")

; Define the right-click menu in the listview.
Menu ItemMenu, Add, Update Project/Context, MenuHandler
Menu ItemMenu, Add, Delete, MenuHandler
Menu ItemMenu, Add, Subtask, MenuHandler
; Define the priority submenu.
Menu, Priority, Add, H, MenuHandler
Menu, Priority, Add, M, MenuHandler
Menu, Priority, Add, L, MenuHandler
Menu, Priority, Add, None, MenuHandler


; Define StartDate submenu
Menu, StartDate, Add, Update Start Date, MenuHandler
Menu, StartDate, Add, Clear, MenuHandler

; Define Due Date submenu
Menu, DueDate, Add, Update Due Date, MenuHandler
Menu, DueDate, Add, Clear, MenuHandler


Menu, Description, Add, Edit, MenuHandler
; Define the priority submenu in the context menu.
Menu, ItemMenu, Add, Priority, :Priority
Menu, ItemMenu, Add, Description, :Description
Menu, ItemMenu, Add, Start Date, :StartDate
Menu, ItemMenu, Add, Due Date, :DueDate



; Define the status submenu.
Menu, Status, Add, Completed, MenuHandler
Menu, Status, Add, Archive, MenuHandler
Menu, ItemMenu, Add, Status, :Status

; Define the annotation submenu.
Menu, Annotation, Add, Show, MenuHandler
Menu, Annotation, Add, Create, MenuHandler
Menu, Annotation, Add, Change, MenuHandler
Menu, Annotation, Add, Remove, MenuHandler
Menu, Annotation, Add, Link, MenuHandler
Menu, Annotation, Add, Local Repository, MenuHandler

; Define the Annotation submenu in the context menu.
Menu, ItemMenu, Add, Annotation, :Annotation

; Define the help in the menubar.
Menu, Help, Add,About CSVTodo, MenuHandler
Menu, Help, Add, Holidays, MenuHandler

; Define the window submenu in the menubar.
StringSplit, FileList, FILE_LIST, |
Loop, %FileList0%
{
Menu, Window, Add, % "&" . A_Index . ": " . FileList%A_Index%, MenuHandler
}

; Define the file menu.
Menu, File, Add, Options, MenuHandler
Menu, File, Add, Open Configuration, MenuHandler
Menu, File, Add, Export, MenuHandler
Menu, File, Add, Project to YAML, MenuHandler
Menu, File, Add, Update done-tasks, MenuHandler
Menu, File, Add, Restart, MenuHandler
Menu, File, Add, Exit, MenuHandler

; Define the menubar.
Menu, MenuBar, Add, File, :File
Menu, MenuBar, Add, Window, :Window
Menu, MenuBar, Add, Help, :Help
; Create the menubar.
Gui, Menu, MenuBar

; Get values from ini file for options Gui.
CLOSE_AFTER_ADDING := GetConfig("UI","CloseAfterAdding","0")
DISPLAY_SUBTASKS := GetConfig("UI","DisplaySubtasks","1")
GUI_FONT := GetConfig("UI","GuiFont","San Serif")
FONT_SIZE := GetConfig("UI","FontSize","9")

COL_1_WIDTH := 250
COL_2_WIDTH := 150
BUTTON_WIDTH := 50

ADD_WIN := 0

StringUpper, SCRIPT_HOTKEY, SCRIPT_HOTKEY

IfInString, SCRIPT_HOTKEY, #
{
    ADD_WIN := 1
    StringReplace, SCRIPT_HOTKEY, SCRIPT_HOTKEY, #, , All
}

; Define options Gui.
Gui, 2:Add, Text, R2 , Full list of options in File-Open Configuration.
Gui, 2:Add, CheckBox, vCloseAfterAdding Checked%CLOSE_AFTER_ADDING% W%COL_1_WIDTH%, Close Gui after adding item.
Gui, 2:Add, Text, Section, Set hotkey to run the script.
Gui, 2:Add, Text, ,

Gui, 2:Add, Text, , Set Gui font.
Gui, 2:Add, Text, , Set Gui font size.
Gui, 2:Add, Hotkey, vScriptHotkey ys W%COL_2_WIDTH%, %SCRIPT_HOTKEY%
Gui, 2:Add, CheckBox, vAddWin W%COL_2_WIDTH% Checked%ADD_WIN%, Add Win key to hotkey.
Gui, 2:Add, Edit, vGuiFont W%COL_2_WIDTH%, %GUI_FONT%
Gui, 2:Add, Edit, W%COL_2_WIDTH%,
Gui, 2:Add, UpDown, vFontSize , %FONT_SIZE%

Gui, 2:Add, Button, gOK Section Default W%BUTTON_WIDTH%, OK
Gui, 2:Add, Button, gCancel ys W%BUTTON_WIDTH%, Cancel

; Show gui.
Gui Show, AutoSize Center, %WINDOW_TITLE%
GuiControl Focus, NewItem
GuiControlGet Filter

ReadFile(Filter, true)

If (ADD_WIN = 1) {
    SCRIPT_HOTKEY := "#" . SCRIPT_HOTKEY
}

StringLower, SCRIPT_HOTKEY, SCRIPT_HOTKEY

; Win+T is the default hotkey.
Hotkey, %SCRIPT_HOTKEY%, ShowGui, On


Return

; Handle when cancel is clicked or when the ESCAPE key is pressed for the options Gui.
2GuiEscape:
Cancel:
Gui, 2:Hide
Return

; Handle when OK is pressed.
OK:
    Gui, 2:Submit

    If (AddWin = 1) {
        ScriptHotkey := "#" . ScriptHotkey
    }

    StringLower, ScriptHotkey, ScriptHotkey

    WriteConfig("UI", "CloseAfterAdding", CloseAfterAdding)
    WriteConfig("UI", "GuiFont", GuiFont)
    WriteConfig("UI", "FontSize", FontSize)
    WriteConfig("UI", "ScriptHotkey", ScriptHotkey)
Reload

ShowGui:
    Gui Show,AutoSize Center, %WINDOW_TITLE%

    GuiControl Focus, NewItem
    GuiControlGet Filter

    ReadFile(Filter, true)

Return

SelectDateTime:
GuiControlGet DateTime
FormatTime, DueDate, %DueDate%, yyyy-MM-dd
FormatTime, StartDate, %StartDate%, yyyy-MM-dd
If (DueDate <> "")
{
Clipboard := DueDate
}
If (StartDate <> "") 
{
Clipboard := StartDate
}
GuiControl,, MyEditControl, 0
Return
ButtonClear:
GuiControl,,Filter,
tr.Clear()
tr.FreeMemory()
return


ButtonISO:
	lineNumber := LV_GetNext()
    if (lineNumber =0) 
    {
    msgbox Select a list row.
    return
    }
    rn := Find_Row_Number(lineNumber)
    CSV_Load(TODO_PATH,"todolist")
    start:= CSV_ReadCell("todolist",rn,CSV_POS_STARTDATE)
    end:= CSV_ReadCell("todolist",rn,CSV_POS_DUEDATE)
    desc:= CSV_ReadCell("todolist",rn,CSV_POS_DESCRIPTION)
    ymn_start := DateParse(start)
    ymn_end := DateParse(end)
    FormatTime, StartISO, %ymn_start% , YWeek
    FormatTime, StartISODay, %ymn_start% , WDay
    FormatTime, EndISO, %ymn_end% , YWeek
    FormatTime, EndISODay, %ymn_end% , WDay
    msg := desc . "`nStart: " . StartISO . "   D" . CorrectISOday(StartISODay) . "`n" . "End:  " . EndISO . "   D" . CorrectISOday(EndISODay) . "`n" . ""
    tr := TextRender(msg, "time:15000 n:false x:right y:top  r:2% c:darkblue s:" . FontSize , "j:left n:1")
return

ButtonDisplay:
	lineNumber := LV_GetNext()
    if (lineNumber =0) 
    {
    msgbox Select a list row.
    return
    }
    rn := Find_Row_Number(lineNumber)
    CSV_Load(TODO_PATH,"todolist")
    selected_desc:= CSV_ReadCell("todolist",rn,CSV_POS_DESCRIPTION)
    selected_project:= CSV_ReadCell("todolist",rn,CSV_POS_PROJECT)
    selected_context := CSV_ReadCell("todolist",rn,CSV_POS_CONTEXT)
    display :=selected_desc . "`nProject: " . selected_project . "`n" . "Context: " . selected_context . "`n" . ""
    tr := TextRender(display, "time:15000 n:false x:right y:top  r:2% c:darkblue s:" . FontSize , "j:left n:1")
    GuiControl,,Filter,%selected_project%
return
; Handle when Today button is click
ButtonToday:

    today := A_Now
    FormatTime, now,%today%, yyyy-MM-dd

    today := "Due Today, " . now . " :`n`n"
    ; due date = today
    CSV_Load(TODO_PATH,"todolist")
    duetoday :=""
    totalRows := CSV_TotalRows("todolist")
    Loop, % totalRows
    {
    row := CSV_SearchColumn("todolist",now,CSV_POS_DUEDATE,A_Index)
    if (row != 0)
    {
    duetoday := duetoday . "Project: " . CSV_ReadCell("todolist",row,CSV_POS_PROJECT) .  "`n" . CSV_ReadCell("todolist",row,CSV_POS_DESCRIPTION) . "`n"


    }

    }
    today := today . duetoday
    tr := TextRender(today, "time:10000 x:right y:top r:2% c:darkblue s:" . FontSize , "j:left n:1")
    return
; Handle when the OK button is clicked or the ENTER key is pressed.
Add:
	GuiControlGet, ActiveControl, FocusV

	; If {ENTER} is pressed while the list view is active, update item.
	If (ActiveControl = "Items") {
		;UpdateItem(LV_GetNext())
		Return
	}

    Gui Submit, NoHide

   if (StartDate <> "") {
        FormatTime, StartDate, %StartDate%, yyyy-MM-dd
      
    }
   if (DueDate <> "") {
        FormatTime, DueDate, %DueDate%, yyyy-MM-dd
      
    }

    If (Project <> "") {
		Project := RegExReplace(Project, "^\+?", "+")
    }

    If (Context <> "") {
		Context := RegExReplace(Context, "^@?", "@")
    }
   if (NewItem="")
   {
    MsgBox, Insert Task Description
    GuiControl, Focus, New
    return
   }
   else
   {
    ; Not a real function just a goto
    if DateSequenceOK()
    {
    AddItem(NewItem, Project, Context, StartDate, DueDate, LineNumber)

    ; Clear the NewItem edit box.
	GuiControl,, NewItem
	GuiControl, Focus, NewItem
    }
}
    ; Find if close after adding option is true.
    If (GetConfig("UI", "CloseAfterAdding", "1"))
        Gui Cancel
    Else
        FilterItems()
Return


; Handle when the filter box changes.
Filter:
    FilterItems()
Return

ToolTipOff:
 Tooltip
return
; Handle listview events.
Items:
	; Get the line number of the selected row.
	lineNumber := LV_GetNext()
    If A_GuiEvent in Normal
            {

        rN :=  Find_Row_Number(lineNumber)
        CSV_Load(TODO_PATH,"todolist")
        proj := CSV_ReadCell("todolist",rN,CSV_POS_PROJECT)
        cont:= CSV_ReadCell("todolist",rN,CSV_POS_CONTEXT)

        tooltip, %proj%%A_Tab%%cont%
        SetTimer, ToolTipOff, -2000
            }

    ; Handle when an item is double clicked.
    If (A_GuiEvent = "DoubleClick") {
		If (lineNumber != 0) {
		

        rN :=  Find_Row_Number(lineNumber)
        CSV_Load(TODO_PATH,"todolist")
        annotation := CSV_ReadCell("todolist",rN,CSV_POS_ANNOTATION)
            if (annotation)
            {
            ; check annotation type
            rowNumber := LV_GetNext()
            LV_GetText(type_note, rowNumber, ANNOTATION_COLUMN)
                try {
                    if (type_note = "N"){
                    cl := TEXT_EDITOR . " '" . annotation . "'"
                    Run, %cl%,, Max
                    }
                    else if (type_note ="R")
                    {
                    Run, %annotation%,, Max
                    }
                    else if (type_note ="L")
                    {
                    Run, %annotation%,, Max
                    }
                    }
                catch
                    {
                    msgbox , Text editor not supported 
                    cl :=  "notepad.exe" . " '" . INI_PATH . "'"
                    Run, %cl%
                    }
             }
            else 
            {
            msgbox , Annotation not found
            }
        }
	}

	; Handle when an item is right-clicked.
	Else If (A_GuiEvent = "RightClick") {
		If (lineNumber != 0) {
			Menu ItemMenu, Show
		}
	}

	Else If (A_GuiEvent = "K") {
		If (lineNumber != 0) {
			; Delete item when {DELETE} is pressed.
			If (A_EventInfo = 46) {

                GuiControl,,Filter ,""
			}

			; Show context menu when {APPSKEY} is pressed.
			Else If (A_EventInfo = 93) {
				Menu ItemMenu, Show
			}

		}
    }
Return

; Handle when an item is selected in the context menu.
MenuHandler:
    If (A_ThisMenuItem = "Delete")
        {   ; Row number in the list
         rowNumber := LV_GetNext()
         rN :=  Find_Row_Number(rowNumber)

	    GetPartsFromRow(rowNumber,text,date,sdate)
        DeleteItem(rN,text)
        }
    Else If (A_ThisMenuItem = "Update Project/Context")
        {
        GuiControlGet, Project
        GuiControlGet, Context

        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        CSV_Load(TODO_PATH,"todolist")
        If (Project <> "")
            CSV_ModifyCell("todolist",Project,rN,CSV_POS_PROJECT)
        If (Context <> "")
            CSV_ModifyCell("todolist",Context,rN,CSV_POS_CONTEXT)
        CSV_Save(TODO_PATH,"todolist")
        LV_Delete()
        FilterItems()
        }
    Else If (A_ThisMenu="Status")
        {
        Global TODO_PATH
        Global ANNOTATION_COLUMN
        item := A_ThisMenuItem
        If (item="Completed")
        {
        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file

        LV_GetText(type_note, rowNumber, ANNOTATION_COLUMN)
        
        rN :=  Find_Row_Number(rowNumber)
        CSV_Load(TODO_PATH,"todolist")
        id := CSV_ReadCell("todolist",rN, CSV_POS_ID)
        match := RegExMatch(id,"O)(\d+)([a-z])",Pat)
        primary_task := id
        unfinished_subtasks := false
        If (match = 0 )
            {
            totalRows := CSV_TotalRows("todolist")
            Loop, % totalRows
                {
                c_id := CSV_ReadCell("todolist",A_Index, CSV_POS_ID)
                If ( c_id = primary_task . "s")
                {

                status := CSV_ReadCell("todolist",A_Index, CSV_POS_STATUS)
                if (status <> "x")
                 unfinished_subtasks := true

                }

                }
            if (unfinished_subtasks)
            {
            MsgBox Subtasks not completed
            }
            Else
            {
            UpdatePriority("None", rowNumber,false)
            UpdateDate("now", rowNumber,false)
            UpdateCheck("x",rowNumber)
            ; create a precommit hook
            if (type_note = "R")
            {
            repository := CSV_ReadCell("todolist",rN, CSV_POS_ANNOTATION)
            githook_commit := repository . "\\.git\\hooks\\post-commit"
            githook_push:= repository . "\\.git\\hooks\\pre-push"
            gittag := RegExReplace(CSV_ReadCell("todolist",rN, CSV_POS_CONTEXT),"@","")
            gitannotation := CSV_ReadCell("todolist",rN, CSV_POS_DESCRIPTION)
            FileDelete %githook_commit%
            FileDelete %githook_post%
            ; TODO select lightweight or annotated
            FileAppend , 
            (
#!/usr/bin/env bash
git tag -a "%gittag%" -m "%gitannotation%"

            ), %githook_commit% 
            FileAppend , 
            (
#!/usr/bin/env bash
rm -f -- %githook_commit%
            ), %githook_push% 

            }
            }
        }  
        Else {
        UpdateDate("now", rowNumber,false)
        UpdatePriority("None", rowNumber,false)
        UpdateCheck("x",rowNumber)
            if (type_note = "R")
            {
            repository := CSV_ReadCell("todolist",rN, CSV_POS_ANNOTATION)
            githook_commit := repository . "\\.git\\hooks\\post-commit"
            githook_push:= repository . "\\.git\\hooks\\pre-push"
            gittag := RegExReplace(CSV_ReadCell("todolist",rN, CSV_POS_CONTEXT),"@","")
            gitannotation := CSV_ReadCell("todolist",rN, CSV_POS_DESCRIPTION)
            FileDelete %githook_commit%
            FileDelete %githook_post%
            ; TODO select lightweight or annotated
            FileAppend , 
            (
#!/usr/bin/env bash
git tag -a "%gittag%" -m "%gitannotation%"

            ), %githook_commit% 
            FileAppend , 
            (
#!/usr/bin/env bash
rm -f -- %githook_commit%
            ), %githook_push% 

            }
        }
     }
    
     Else IF (item = "Archive")
     {
        rowNumber := LV_GetNext()
        ; Row number in the csv file

        rN :=  Find_Row_Number(rowNumber)
        CSV_Load(TODO_PATH,"todolist")
        id := CSV_ReadCell("todolist",rN, CSV_POS_ID)
        match := RegExMatch(id,"O)(\d+)([a-z])",Pat)
        primary_task := id

        primary_status := CSV_ReadCell("todolist",rN,CSV_POS_STATUS)
        unfinished_subtasks := false
        If (match = 0 and primary_status ="x")
        ;task without subtasks or primary_task
        {
            totalRows := CSV_TotalRows("todolist")
            Loop, % totalRows
            {
                c_id := CSV_ReadCell("todolist",A_Index, CSV_POS_ID)
                If ( c_id = primary_task . "s")
                {
                    status := CSV_ReadCell("todolist",A_Index, CSV_POS_STATUS)
                    if (status <> "x")
                    {
                     unfinished_subtasks := true
                    }
                }
            }
            if (unfinished_subtasks)
                {
                MsgBox Subtasks not completed.
                }
            Else
                {
                totalRows := CSV_TotalRows("todolist")
                    Loop, % totalRows
                    {
                        c_id := CSV_ReadCell("todolist",A_Index, CSV_POS_ID)
                        If ( c_id = primary_task . "s" or c_id = primary_task)
                        {
                        CSV_ModifyCell("todolist","a", A_Index,CSV_POS_STATUS)
                        }
                    }
                CSV_Save(TODO_PATH,"todolist")
                LV_Delete()
                FilterItems()
                }
         } 
        
        Else {
            MsgBox Primary Task not completed.
         }
    }
    }
    Else If (A_ThisMenu="Description"){
        Global TODO_PATH
        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        CSV_Load(TODO_PATH,"todolist")
        LV_GetText(text, rowNumber, TEXT_COLUMN)
        InputBox, desc, Edit Description,, , , , , , , , %text%
        if (desc ="")
        {
        MsgBox Description cannot be empty
        }
        else
        {
        CSV_ModifyCell("todolist",desc,rN,CSV_POS_DESCRIPTION)

        CSV_Save(TODO_PATH,"todolist")
        }
        LV_Delete()
        FilterItems()
    }
    Else If (A_ThisMenu = "Priority") {
        rowNumber := LV_GetNext()
        item := A_ThisMenuItem
        UpdatePriority(item, rowNumber,true)

    }
    Else If (A_ThisMenuItem = "Subtask") {

        Global TODO_PATH
        ; Row number in the list
        rowNumber := LV_GetNext()

        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        item := A_ThisMenuItem
        CSV_Load(TODO_PATH,"todolist")
        id := CSV_ReadCell("todolist",rN, CSV_POS_ID)
        completed := CSV_ReadCell("todolist",rN, CSV_POS_STATUS)
        match := RegExMatch(id,"O)(\d+)([a-z])",Pat)
        If (match = 0 and completed <> "x")
        ;task without subtasks
        {
        row_to_clone := CSV_ReadRow("todolist", rN)
        newRow := ""
        Loop, Parse, row_to_clone, % ","
         {
            if A_Index = 1
                newRow := newRow . A_LoopField . "s"
            else if A_Index = 3
                newRow := newRow . ","
            else if A_Index = 5
                {
                desc := A_LoopField . " [S]"

                InputBox, newdesc, Edit Description,, , , , , , , , %desc% 
                if (newdesc ="")
                {
                    newdesc ="Not defined"
                 }   
                
                newRow := newRow . "," . newdesc
		
                }
           else if A_Index = 7
                newRow := newRow . ","
           else if A_Index = 8
                newRow := newRow . ","
            else
                newRow := newRow . "," . A_LoopField
         }

            CSV_AddRow("todolist",newRow)
            lastline := CSV_TotalRows("todolist")
            foc := LV_GetNext()
            ;lastline += 1
            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
            LV_Modify(foc, "+Select") ; select


        }
Else IF (completed ="x")
{

        MsgBox, Adding a new subtask to a completed task, not possible.
}
Else
{

        MsgBox, Adding a new subtask to an existing subtask, not possible. Subtasks must be created from a primary task.
    }


}
    Else If (A_ThisMenu="StartDate"){
        Global TODO_PATH
        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        item := A_ThisMenuItem
        CSV_Load(TODO_PATH,"todolist")

        If (item="Update Start Date"){

            GuiControlGet, StartDate
            FormatTime, StartDate, %StartDate%, yyyy-MM-dd
            CSV_ModifyCell("todolist",StartDate,rN,CSV_POS_STARTDATE)

        }
        Else If (item = "Clear"){

            CSV_ModifyCell("todolist","",rN,CSV_POS_STARTDATE)
        }

            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
    }
    Else If (A_ThisMenu="DueDate"){
        Global TODO_PATH
        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        item := A_ThisMenuItem
        CSV_Load(TODO_PATH,"todolist")

        If (item="Update Due Date"){

            GuiControlGet, DueDate
            if DateSequenceOK()
            {
            FormatTime, DueDate, %DueDate%, yyyy-MM-dd
            CSV_ModifyCell("todolist",DueDate,rN,CSV_POS_DUEDATE)
            }
            else
            {
            return
            }
        }
        Else If (item = "Clear"){

            CSV_ModifyCell("todolist","",rN,CSV_POS_DUEDATE)
        }

            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
    }
    Else If (A_ThisMenu ="Annotation") {
        Global TEXT_EDITOR
        Global ANNOTATION_COLUMN
        ; Row number in the list
        rowNumber := LV_GetNext()
        ; Row number in the csv file
        rN :=  Find_Row_Number(rowNumber)
        item := A_ThisMenuItem
        CSV_Load(TODO_PATH,"todolist")
        If (item ="Show"){
            annotation := CSV_ReadCell("todolist",rN,CSV_POS_ANNOTATION)
            LV_GetText(type_note, rowNumber, ANNOTATION_COLUMN)
            ; type of note
            ; L hyperlink
            ; R local repository
            ; N note text file

            if (type_note="N" ){
            cl := TEXT_EDITOR . " " . annotation 
            Run, %cl%,, Max
            }
            else{
            Run, %annotation%,, Max
            }
        }

        Else If (item = "Create"){

             note_title:= CSV_ReadCell("todolist",rN,CSV_POS_DESCRIPTION) 
             filename := RegexReplace(note_title, "\:", " ") 
             project_tag:= CSV_ReadCell("todolist",rN,CSV_POS_PROJECT) 
             context_tag:= CSV_ReadCell("todolist",rN,CSV_POS_CONTEXT) 
             todo_desc:= CSV_ReadCell("todolist",rN,CSV_POS_DESCRIPTION) 


             if (project_tag != "")
             { 
             project_tag:= RegexReplace(project_tag, "\+", "")
             }


             filename_path := ANNOTATION_PATH . "\" . filename . ".txt"
             
            CSV_ModifyCell("todolist",filename_path,rN,CSV_POS_ANNOTATION)
            CSV_Save(TODO_PATH,"todolist")
             If (!FileExist(filename_path))
             {
             FileAppend, 
             (
%note_title%  

@todo_annotation
@project_%project_tag%
%context_tag%

TODO %todo_desc%
            
             ), %filename_path%
             
            LV_Delete()
            FilterItems()
            }
            
            cl := TEXT_EDITOR . " " . filename_path 
            Run, %cl%
        }
        Else If (item = "Change"){
            FileSelectFile, SelectedFile, 3, , Select a text file (*.txt)
            if (SelectedFile = "")
	        return
            CSV_ModifyCell("todolist",SelectedFile,rN,CSV_POS_ANNOTATION)
            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
        }
        Else If (item = "Remove"){
            ; Todo confirm msgbox
            MsgBox, 4,, Do you want to delete the note file? (press Yes or No)
            annotation := CSV_ReadCell("todolist",rN,CSV_POS_ANNOTATION)
            IfMsgBox Yes
            { 
            FileDelete %annotation%
            }
            CSV_ModifyCell("todolist","",rN,CSV_POS_ANNOTATION)
            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
            ; ask to remove file


        }
        Else if (item = "Link")
        {

            InputBox, link, "Enter link or email address",, , , , , , , ,
            if ErrorLevel
                return
            else
            {
            CSV_ModifyCell("todolist",link,rN,CSV_POS_ANNOTATION)
            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
            }
        }

        Else if (item = "Local Repository")
        {
            FileSelectFolder, LocalRepo
            If( !InStr( FileExist(LocalRepo . "\\.git"), "D") )
	            {
                msgbox Not a Git Repo
                return 
                }
            CSV_ModifyCell("todolist",LocalRepo,rN,CSV_POS_ANNOTATION)
            CSV_Save(TODO_PATH,"todolist")
            LV_Delete()
            FilterItems()
        }
        }

    Else If (A_ThisMenu = "Help") {
        If (A_ThisMenuItem = "About CSVTodo")
            Run, %A_ScriptDir%\help.exe
        Else IF (A_ThisMenuItem = "Holidays")
        {
        calendar := GetConfig("LOCALE","Calendar","EU")
        sqlCMD := A_ScriptDir . "\\query_sqlite.exe " . A_ScriptDir . "\\calendar" . calendar . ".db " . """SELECT  YEAR || '-' || MONTH || '-' || DAY || '     ' || ISOYEAR||  ' W' ||ISOWEEK || '  '  || HOLIDAYDESC from calendar where YEAR=" . A_YEAR . " and ISHOLIDAY=1;"
        MsgBox % RunWaitOne(sqlCMD)
        }

    }
    Else If (A_ThisMenu = "Window") {
        StringTrimLeft, item, A_ThisMenuItem, 3
       
        TODO_PATH := GetPath(item, item)
        FilterItems()
    }
    Else If (A_ThisMenu = "File") {
        If (A_ThisMenuItem = "Options") {
            Gui, 2:Show,, Options
        }
        Else If (A_ThisMenuItem = "Open Configuration") {
          
            cl := "notepad.exe" . " '" . INI_PATH . "'"
            Run, %cl%
            
        }
        Else If (A_ThisMenuItem = "Exit") {
            ExitApp
        }
        Else If (A_ThisMenuItem ="Restart")
        {
            Reload
        }
        Else If (A_ThisMenuItem = "Update done-tasks") {
; Move archived to done.csv
        CSV_Load(TODO_PATH,"todolist")
        CSV_Load(DONE_PATH,"donelist")
        totalRows := CSV_TotalRows("todolist")
        timestamp := A_Now
        newRow :=""
            Loop, % totalRows
            {
            B_index := A_Index
                    status := CSV_ReadCell("todolist",B_index, CSV_POS_STATUS)
                    if (status = "a")
                    {
                    row:= CSV_ReadRow("todolist",B_index)
                    ; Todo change id to A_Now

                    Loop, Parse, row, % ","
                     {
                        if A_Index = 1
                            newRow := newRow . timestamp 
                        else
                            newRow := newRow . "," . A_LoopField
                     }
                    CSV_AddRow("donelist",newRow)
                    newRow := "" ; restore new row
             
                    }
             }


            A:=1
            Loop, % totalRows
            {
                    status := CSV_ReadCell("todolist",A, CSV_POS_STATUS)
                    if (status = "a")
                    {
                    CSV_DeleteRow("todolist",A)
                    }
                    else 
                    { 
                    A:= A+1
                    }
             }       
            CSV_Save(TODO_PATH,"todolist")

        CSV_Save(DONE_PATH,"donelist")
        LV_Delete()
        FilterItems()
        }
        Else If (A_ThisMenuItem = "Export") {
            Run, %TODO_PATH%
        }

        Else If (A_ThisMenuItem = "Project to YAML") {
            ; check if project not empty
            GuiControlGet, Project
            if (Project = ""){
              MsgBox, Select a Project
            }
            else {
            MsgBox, Not yet implemented

            }


        }
    }
Return

; Handle when the X is clicked or when the ESCAPE key is pressed.
GuiClose:
GuiEscape:
    Gui Cancel
Return

; Handle when the GUI is resized 
; Doesn't seem to work at all ?
GuiSize:
    ; Resize controls relative to the window.
    ;Anchor("NewItem", "w")
    ;Anchor("Context", "w")
    ;Anchor("Filter", "w")
    Anchor("Items", "wh",true)
    Anchor("Add", "y")
Return

; Filters the items displayed in the list view.
FilterItems() {
    GuiControlGet Filter
    ReadFile(Filter, false)
}


Find_Row_Number(LV_Row){
    ; From the Row Number in the Gui table
    ; function return the row number in the
    ; csv file

    Global TODO_PATH
    Global TEXT_COLUMN
    Global SUBTASK_COLUMN
    Global CSV_POS_ID
    CSV_Load(TODO_PATH,"todolist")

    LV_GetText(text, LV_Row, TEXT_COLUMN)
    LV_GetText(id, LV_Row, SUBTASK_COLUMN)

    Rows:=CSV_TotalRows("todolist")
    Loop, % Rows
    {
    found:=CSV_MatchCell("todolist", text, A_Index)
          if found=0 
          {

              break
          }
     
    Results := found
    }
    Loop, Parse, Results, % ","
    {
        if A_Index = 1
            rtnRow := A_LoopField
    }

    return rtnRow
   }
; Read the todo.csv file into the GUI.
; Filters items based on the filter field.
ReadFile(filter, refreshFilter) {
    Global TODO_PATH
    Global TEXT_COLUMN
    Global SUBTASK_CHAR
    Global LINE_COLUMN
    Global CSV_POS_STATUS
    Global CSV_POS_DESCRIPTION
    Global CSV_POS_DUEDATE 
    Global CSV_POS_STARTDATE 
    Global CSV_POS_ID
    Global CSV_POS_PRIORITY
    Global CSV_POS_ANNOTATION
    Global CSV_POS_PROJECT
    Global CSV_POS_CONTEXT
    Global CSV_POS_DUEDATE 

	GuiControlGet, ShowSubtask
	GuiControlGet, Project
	GuiControlGet, Context

	; Add "+" and "@" to project or context if needed.
	Project := RegExReplace(Project, "^\+?", "+")
	Context := RegExReplace(Context, "^@?", "@")

	; Use these variables to keep track of what contexts and projects have been added.
	projectsAdded := Project <> "+" ? "|" . Project . "||" : "|"
	contextsAdded := Context <> "@" ? "|" . Context . "||" : "|"

	; Clear the combo boxes.
	GuiControl ,, Project, ||
	GuiControl ,, Context, ||

    ; Disable notifications for checking and unchecking while the list is populated.
    GuiControl, -AltSubmit, Items

    lineNumber := 1

    ; Active color change and disable redraw.
    ;LV_Change(1, 1, 1, LINE_COLUMN)
    GuiControl, -Redraw, Items

    If (refreshFilter) {
        filter := ""
    }

    ; Clear the list view.
    LV_Delete()

    CSV_Load(TODO_PATH,"todolist")
    Number_of_rows := CSV_TotalRows("todolist")
    Loop %Number_of_rows%
    {
        ;filter header
        If (lineNumber > 1) {

			; Find projects to add to combo boxes.
            projectPart := CSV_ReadCell("todolist",lineNumber, CSV_POS_PROJECT)
			If (projectPart <> "") {
				If (InStr(projectsAdded, "|" . projectPart . "|") = 0) {
					projectsAdded := projectsAdded . projectPart . "|"
				}
			}
            ;context col
            contextPart := CSV_ReadCell("todolist",lineNumber,CSV_POS_CONTEXT)


            ; Find contexts to add to combo boxes. Not working for more than one context


            If (contextPart <> "") {
                If (InStr(contextsAdded, "|" . contextPart . "|") = 0) {
                    contextsAdded := contextsAdded . contextPart . "|"
                }
            }
            
            subtask := CSV_ReadCell("todolist",lineNumber,CSV_POS_ID)
            textPart := CSV_ReadCell("todolist",lineNumber,CSV_POS_DESCRIPTION)
            datePart := CSV_ReadCell("todolist",lineNumber,CSV_POS_DUEDATE)
            startdate := CSV_ReadCell("todolist",lineNumber,CSV_POS_STARTDATE)
            donePart := CSV_ReadCell("todolist",lineNumber,CSV_POS_STATUS)
            priority := CSV_ReadCell("todolist",lineNumber,CSV_POS_PRIORITY)
            annotation := CSV_ReadCell("todolist",lineNumber,CSV_POS_ANNOTATION)

            p := RegExMatch(annotation, "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
            if  (p=0){

                SplitPath, annotation, OutFileName, OutDir, OutExtension, annotation_fn, OutDriv
                    if (annotation_fn != ""){

                    if !OutExtension 
                        annotation_fn := "R"
                    else 
                        annotation_fn := "N"
                        }
            }
            else 
            {
                annotation_fn := "L"
            }


        ; determine value of the priority
        If (donePart ="x")
            Priority_val :="*"
        Else If (priority = "H")
            Priority_val := "*****"
        Else if (priority= "M")
            Priority_val :="****"
        Else if (priority = "L")
            Priority_val :="***"
        Else if (priority = "" )
            Priority_val :="**"
       
        duedate := datePart
        today := A_Now
        FormatTime, now,%today%, yyyy-MM-dd

       If (now=duedate and donePart <>"x")
        {
            Priority_val := Priority_val . "***"
        }
        today += 1,Days
        FormatTime, tommorow,%today%,yyyy-MM-dd
    
         If (tommorow = duedate  and donePart <>"x")
         {     
         Priority_val := Priority_val . "**"
         }

        today += 1,Days
        FormatTime, day_after_tommorow,%today%,yyyy-MM-dd
        
         If (day_after_tommorow = duedate and donePart <>"x")
         {     
                Priority_val := Priority_val . "*"
         }
        status := ""
        today := A_Now
        FormatTime, now,%today%, yyyy-MM-dd
            If (donePart = "" and ( datePart ="" and startdate="") )
{

                status:= "Undefined"
                Priority_val := "*"
}
            Else if (donePart ="" and datePart<>"" and now>duedate)
                {
                status:= "Overdue"
                Priority_val := Priority_val . "*****"
                }
            Else if (donePart ="" and startdate<>"" and now < startdate)
                    status:= "Pending"
            Else if (donePart ="" and datePart<>"" and now <= duedate)
                    {
                    status:= "In Progress"
                    Priority_val := Priority_val . "****"
                    }
            
            If (donePart ="a")
                status:= "Archived"
            If (donePart ="x")
                status:= "Completed"

            line := textPart . projectPart . contextPart . status

            If (filter <> "")
            {
            IfInString, line, %filter%
            {
            
            AddItemToList(priority,subtask,status,textPart,startdate,datePart, annotation_fn, lineNumber,Priority_val)

            }
            } Else {

            AddItemToList(priority,subtask,status,textPart,startdate,datePart, annotation_fn, lineNumber,Priority_val)
            }
            
        }
        lineNumber := lineNumber + 1
    }

    ; Re-select the values that were previously selected.
    GuiControl ChooseString, Filter, %filter%

    ; Add the projects and contexts that were found.
    GuiControl ,, Context, %contextsAdded%
    GuiControl ,, Project, %projectsAdded%

    ; Re-enable notifications for handling checking and unchecking.
    GuiControl, +AltSubmit, Items

    ; Re-enable redraw.
    GuiControl, +Redraw, Items

    ; Sort list
    ;LV_ModifyCol(8, "SortDesc")
    ; Resize columns in list view to fit their contents.
    ;LV_ModifyCol()
    ;hide line number and priority calc
    LV_ModifyCol(9, "SortDesc")
    LV_ModifyCol(9, 0)
    LV_ModifyCol(5, 0)
    LV_ModifyCol(1, 100)
    LV_ModifyCol(6, 110)
    LV_ModifyCol(7, 110)
    LV_ModifyCol(4, 220)
}


; Add an item to the list view.
AddItemToList(priority,subtask,donePart, textPart,ByRef startdate, ByRef datePart, annotation, lineNumber,Priority_val) {

 LV_Insert(1,"", donePart, priority,subtask, textPart, lineNumber, startdate,datePart , annotation,Priority_val)
}

; Parse a line from todo.txt.
ParseLine(line, ByRef donePart, ByRef textPart, ByRef datePart) {
	donePart := TrimWhitespace(donePart)
	textPart := TrimWhitespace(textPart)
	datePart := TrimWhitespace(datePart)
}

; Add an item to todo.csv.
AddItem(NewItem, Project, Context, StartDate, DueDate, LineNumber) {
    Global TODO_PATH
    Global CSV_POS_ID

    CSV_Load(TODO_PATH,"todolist")
    totalRows := CSV_TotalRows("todolist")

    ; clean description for special characters
    Comma := ","
    StringReplace, NewItem, NewItem, %Comma%,, All

    MaxId := 2
    Loop, % totalRows
    {
    if (A_Index > 1)
                {
                 
                  id :=CSV_ReadCell("todolist",A_Index,CSV_POS_ID)
                  match := RegExMatch(id,"O)(\d+)([a-z])",Pat)
                                                                                                                    
                  if ( id > MaxId and match = 0)
                  {
                 
                  MaxId := id
                  }
                }
    }
    MaxId:=MaxId+1
    CSV_AddRow("todolist", MaxId . ",,," . Project . "," . NewItem . "," . Context . "," . StartDate "," . DueDate . ",")

    CSV_Save(TODO_PATH,"todolist")
    FilterItems()
}

; Change the text of an item.
UpdateItem(rowNumber) {
    Global UPDATE_PROMPT
    Global LINE_COLUMN

	If (! rowNumber) {
		Return
	}

    FilterItems()
}


UpdateDate(newDate, rowNumber, update_lv) {
	Global TEXT_COLUMN
	Global LINE_COLUMN
	Global TODO_PATH
    CSV_Load(TODO_PATH,"todolist")
    If (newDate = "now") {
        now := A_Now
        FormatTime, newDate ,%now%,yyyy-MM-dd
    }

    ; Row number in the csv file
    rN :=  Find_Row_Number(rowNumber)

    CSV_ModifyCell("todolist", newDate,rN, CSV_POS_DUEDATE)
    CSV_Save(TODO_PATH,"todolist")
    if (update_lv){
    LV_Delete()
    FilterItems()
}
}
; Changes the priority of an item when it is selected from the menu.
UpdatePriority(newPriority, rowNumber, update_lv) {
	Global TEXT_COLUMN
	Global LINE_COLUMN
	Global TODO_PATH
    Global CSV_POS_PRIORITY
    CSV_Load(TODO_PATH,"todolist")
    If (newPriority = "None") {
        newPriority := ""
    }

    ; Row number in the csv file
    rN :=  Find_Row_Number(rowNumber)
    CSV_ModifyCell("todolist", newPriority, rN, CSV_POS_PRIORITY)
    CSV_Save(TODO_PATH,"todolist")
    if (update_lv){
    LV_Delete()
    FilterItems()
}
}
; Update check/uncheck task
UpdateCheck(newCheck, rowNumber) {
	Global TEXT_COLUMN
	Global LINE_COLUMN
	Global TODO_PATH
    Global CSV_POS_STATUS
    CSV_Load(TODO_PATH,"todolist")

    ; Row number in the csv file
    rN :=  Find_Row_Number(rowNumber)
    CSV_ModifyCell("todolist", newCheck , rN , CSV_POS_STATUS)
    CSV_Save(TODO_PATH,"todolist")

    LV_Delete()
    FilterItems()

}



; Delete an item.
DeleteItem(rowNumber,text) {
    Global DELETE_PROMPT
    Global TODO_PATH
    Global CSV_POS_ID

	If (! rowNumber) {
		Return
	}
    CSV_Load(TODO_PATH,"todolist")
    StringReplace prompt, DELETE_PROMPT, `%text`%, %text%
    MsgBox 4,, %prompt%

    IfMsgBox No
        Return

    ; TODO check if a main task or a subtask

    id := CSV_ReadCell("todolist",rowNumber, CSV_POS_ID)
    match := RegExMatch(id,"O)(\d+)([a-z])",Pat)
        If (match = 0 )
        ;task without subtasks
        {
        ;Delete the subtasks
        Rows:=CSV_TotalRows("todolist")
        found:=CSV_MatchCell("todolist", id . "s" , CSV_POS_ID)
                 Loop, % Rows
                 {
                  if (found <> "0")
                  {

                      Loop, Parse, found, % ","
                           If A_Index = 1
                             CSV_DeleteRow("todolist", A_LoopField)
                  }
                  
        found:=CSV_MatchCell("todolist", id . "s" , A_Index)

                 }

        CSV_DeleteRow("todolist", rowNumber)

        }
        Else
        {

            annotation := CSV_ReadCell("todolist",rowNumber,CSV_POS_ANNOTATION)
            CSV_DeleteRow("todolist", rowNumber)
        }   
    CSV_Save(TODO_PATH,"todolist")
    FilterItems()
}



; Gets the text and date for the specified row.
GetPartsFromRow(rowNumber, ByRef text, ByRef date, ByRef sdate) {
    Global TEXT_COLUMN
    Global DUE_DATE_COLUMN
    Global START_DATE_COLUMN
    ; Global NONE_TEXT

    LV_GetText(text, rowNumber, TEXT_COLUMN)
    LV_GetText(date, rowNumber, DUE_DATE_COLUMN)
    LV_GetText(sdate, rowNumber, START_DATE_COLUMN)
}

; Remove white spacer from the beginning and end of the string.
TrimWhitespace(str) {
    Return RegExReplace(str, "(^\s+)|(\s+$)")
}

GetPath(key, default) {
    Global HOMEPATH
    Global Header
    folder := GetConfig("Files", "Folder", "\Documents")
    folder := HOMEPATH . folder
    ;folder := ExpandEnvironmentStrings(folder)

    If (!InStr(default, ".csv", true)) {
        default := default . ".csv"
    }

    value := GetConfig("Files", key, TrimWhitespace(default))
    ;value := ExpandEnvironmentStrings(value)

    If (IsAbsolute(value))
        filename := value
    Else
        filename := folder . "\" . value

    If (!FileExist(filename))
        FileAppend,%Header%,%filename%

    return filename
}

; Read ini file.
GetConfig(section, key, default) {
    Global INI_FILE_NAME
    IniRead value, %A_ScriptDir%\%INI_FILE_NAME%, %section%, %key%, %default%
    return value
}

; Write to ini file.
WriteConfig(section, key, value) {
    Global INI_FILE_NAME
    IniWrite, %value%, %A_ScriptDir%\%INI_FILE_NAME%, %section%, %key%
    Return
}

ExpandEnvironmentStrings(str) {
    VarSetCapacity(dest, 2000)
    DllCall("ExpandEnvironmentStrings", "str", str, "str", dest, int, 1999, "Cdecl int")
    Return dest
}

IsAbsolute(path) {
    Return RegExMatch(path, "(^[a-zA-Z]:\\)|(^\\\\)") > 0
}

DateSequenceOK(){
    GuiControlGet, DueDate
    GuiControlGet, StartDate
    FormatTime, dd , %DueDate% , yyyyMMdd
    FormatTime, sd, %StartDate%, yyyyMMdd
    if (sd>dd)
    {
    MsgBox, Due date  %DueDate% cannot be set before Start date %StartDate% 
    return false
    }
    return true
}

EventHandler: ; This label is launched when the contents of the edit has changed
    GuiControlGet,value,,MyEditControl
    GuiControlGet, DueDate
    GuiControlGet, StartDate
    FormatTime, dd , %DueDate% , yyyyMMdd
    FormatTime, sd, %StartDate%, yyyyMMdd
    If value > 0
    {
        
         
        WW := GetConfig("LOCALE","WorkingWeek",1)
        calendar := GetConfig("LOCALE","Calendar","EU")

        if WW = 1
            sqlCMD := A_ScriptDir . "\\query_sqlite.exe " . A_ScriptDir . "\\calendar" . calendar . ".db " . """SELECT * FROM ( SELECT id AS days from calendar where id>=" . sd . " and ISHOLIDAY=0 and ISODAY <> 6 and ISODAY <>7 LIMIT " . value . ") ORDER BY days DESC LIMIT 1;"
        else if WW = 2
            sqlCMD := A_ScriptDir . "\\query_sqlite.exe " . A_ScriptDir . "\\calendar" . calendar . ".db " . """SELECT * FROM ( SELECT id AS days from calendar where id>=" . sd . " and ISHOLIDAY=0 and ISODAY <>7 LIMIT " . value . ") ORDER BY days DESC LIMIT 1;"
        else if WW = 3
            sqlCMD := A_ScriptDir . "\\query_sqlite.exe " . A_ScriptDir . "\\calendar" . calendar . ".db " . """SELECT * FROM ( SELECT id AS days from calendar where id>=" . sd . " and ISHOLIDAY=0  LIMIT " . value . ") ORDER BY days DESC LIMIT 1;"
        else
            return

        GuiControl,, DueDate , % RunWaitOne(sqlCMD)
    }
return


RunWaitOne(command) {
    shell := ComObjCreate("WScript.Shell")
    ; Execute a single command via cmd.exe
    exec := shell.Exec(ComSpec " /C " command)
    ; Read and return the command's output
    return exec.StdOut.ReadAll()
}
DateParse(str) {
	static e2 = "i)(?:(\d{1,2}+)[\s\.\-\/,]+)?(\d{1,2}|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w*)[\s\.\-\/,]+(\d{2,4})"
	str := RegExReplace(str, "((?:" . SubStr(e2, 42, 47) . ")\w*)(\s*)(\d{1,2})\b", "$3$2$1", "", 1)
	If RegExMatch(str, "i)^\s*(?:(\d{4})([\s\-:\/])(\d{1,2})\2(\d{1,2}))?"
		. "(?:\s*[T\s](\d{1,2})([\s\-:\/])(\d{1,2})(?:\6(\d{1,2})\s*(?:(Z)|(\+|\-)?"
		. "(\d{1,2})\6(\d{1,2})(?:\6(\d{1,2}))?)?)?)?\s*$", i)
		d3 := i1, d2 := i3, d1 := i4, t1 := i5, t2 := i7, t3 := i8
	Else If !RegExMatch(str, "^\W*(\d{1,2}+)(\d{2})\W*$", t)
		RegExMatch(str, "i)(\d{1,2})\s*:\s*(\d{1,2})(?:\s*(\d{1,2}))?(?:\s*([ap]m))?", t)
			, RegExMatch(str, e2, d)
	f = %A_FormatFloat%
	SetFormat, Float, 02.0
	d := (d3 ? (StrLen(d3) = 2 ? 20 : "") . d3 : A_YYYY)
		. ((d2 := d2 + 0 ? d2 : (InStr(e2, SubStr(d2, 1, 3)) - 40) // 4 + 1.0) > 0
			? d2 + 0.0 : A_MM) . ((d1 += 0.0) ? d1 : A_DD) . t1
			+ (t1 = 12 ? t4 = "am" ? -12.0 : 0.0 : t4 = "am" ? 0.0 : 12.0) . t2 + 0.0 . t3 + 0.0
	SetFormat, Float, %f%
	Return, d
}

CorrectISOday(day){
    if day > 1
        return day-1
    else 
        return 7

}


dist: help.ahk csvtodo.ahk
	- rm -f dist/*.exe
	- mkdir dist
	- cp ./csvtodo.ico ./dist/
	- cp ./csvtodo.ini ./dist/
	- cp ./csvtodo.png ./dist/
	- cp ./query_sqlite.exe ./dist/
	- cp ./calendarEU.db ./dist/
	- cp ./calendarUS.db ./dist/
	- start help_Compile.ahk
	- start csvtodo_Compile.ahk

clean:
	-rm -rf dist
